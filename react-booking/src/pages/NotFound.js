import { Link } from "react-router-dom";
// import Banner from '../components/Banner'


export default function NotFound() {
    return (
        <div>
            <h1>Page Not Found</h1>
            <p>Go back to the <Link to='/'>homepage.</Link></p>

        </div>
        
    )
}