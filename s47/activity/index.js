console.log(document.querySelector("#txt-first-name"));

console.log(document);

console.log(document.getElementById("txt-first-name"));

// -------------------------------EVENT LISTENERS-------------------------------

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

console.log(txtFirstName.value);
console.log(spanFullName);



txtFirstName.addEventListener("keyup", printLastName);

txtLastName.addEventListener("keyup", printLastName);

function printLastName(event){
	spanFullName.innerHTML = txtFirstName.value +" "+txtLastName.value;
}
