// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
/*
	Syntax:
		fetch("url", {options})
		url - this is the address which the request is to be made and source where the response will come from (endpoint)
		url - this is the address which the request is to be made and source where the response will come from (endpoint)
*/

// Get Post data/ Retrieve/Read Functions
fetch("https://jsonplaceholder.typicode.com/posts")
	.then(res => res.json())
	.then(data => showPosts(data));

// View post - to display each post from JSON placeholder
const showPosts = posts =>{
	// create a variable that  will contain all the posts
	let postEntries = "";
	posts.forEach(post => {
		//Create a variable that will contain all the posts
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}"> ${post.body}</p>
				<button id="post-edit-btn-body-${post.id}" onclick="editPost(${post.id})">Edit</button>
				<button id="post-delete-btn-body-${post.id}" onclick="deletePost(${post.id})">Delete</button>
			</div>	
		`
	});

	// To check what is stored in the postEntries variables.
	console.log(postEntries);

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Mini activity
// show single post
fetch("https://jsonplaceholder.typicode.com/posts/2")
	.then(res => res.json())
	.then(data => console.log(data));
fetch("https://jsonplaceholder.typicode.com/posts/3")
	.then(res => res.json())
	.then(data => console.log(data));

// Post Data / Create Function
document.querySelector("#form-add-post").addEventListener("submit", event => {
	// Prevents the page from reloading. Also prevents the default behavior of our event.
	event.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts",
		{
			method : "POST",
			body : JSON.stringify({
				title : document.querySelector("#txt-title").value,
				body : document.querySelector("#txt-body").value,
				useId : 290
			}),
			headers : {
				"Content-Type" : "application/JSON"
			}
		}).then(res=>res.json())
			.then(data => {
				console.log(data);
				alert("Post successfully added!");
			})
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;

});

// Edit post data/ Edit button

const editPost = id =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;


	document.querySelector("#btn-submit-update").removeAttribute("disabled");

}

// Update post data / PUT method

document.querySelector("#form-edit-post").addEventListener("submit", e => {
	e.preventDefault();

	let id = document.querySelector("#txt-edit-id").value;

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
		{
			method : "PUT",
			body : JSON.stringify({
				id : id,
				title : document.querySelector("#txt-edit-title").value,
				body : document.querySelector("#txt-edit-body").value,
				userId : 290
			}),
			headers : {
				"Content-Type" : "application/JSON"
			}
		}).then(res => res.json())
			.then(data=>{
				console.log(data);
				alert("Post successfully updated!")
			});

	// Reset input fields once submitted
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;
	// Resetting disabled attribute for button
	document.querySelector("#btn-submit-update").setAttribute("disabled", true)
})

// Delete post
const deletePost = id => {
	fetch( `https://jsonplaceholder.typicode.com/posts/${id}`,
		{
			method : "DELETE"
		}
		).then(res => res.json())
			.then(data=>{
				console.log("Post successfully deleted")
			});

			/*let title = document.querySelector(`#post-title-${id}`);
			let body = document.querySelector(`#post-body-${id}`);
			let edit = document.querySelector(`#post-edit-btn-body-${id}`);
			let del = document.querySelector(`#post-delete-btn-body-${id}`);

			title.remove();
			body.remove();
			edit.remove();
			del.remove();*/

			document.getElementById(`post-${id}`).remove();


}