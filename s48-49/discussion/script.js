
// Mock data = will contain all the posts
let posts = [];
// will be the id
let count = 1;

// CREATE (add post)

document.querySelector("#form-add-post").addEventListener("submit", e =>{
	e.preventDefault();

	posts.push(
		{
			id : count,
			title : document.querySelector("#txt-title").value,
			body : document.querySelector("#txt-body").value,
		});
	// next available id (incremental value for id)
	count++;

	showPosts(posts);
	alert("Succefully posted!");
})

// RETRIEVE (show posts)

const showPosts = posts =>{
	// create a variable that  will contain all the posts
	let postEntries = "";
	posts.forEach(post => {
		//Create a variable that will contain all the posts
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}"> ${post.body}</p>
				<button id="post-edit-btn-body-${post.id}" onclick="editPost(${post.id})">Edit</button>
				<button id="post-delete-btn-body-${post.id}" onclick="deletePost(${post.id})">Delete</button>
			</div>		
		`
	});

	// To check what is stored in the postEntries variables.
	console.log(postEntries);

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

const editPost = id =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;


	document.querySelector("#btn-submit-update").removeAttribute("disabled");

}

document.querySelector("#form-edit-post").addEventListener("submit", e => {
	e.preventDefault();

	// loop over the array too check for the id that is same with the element's id and text edit field
	for(let i = 0; i<posts.length; i++){
		// The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
		// Therefore, it is necesary to convert the Number to a String first
		if(posts[i].id.toString()===document.querySelector("#txt-edit-id").value){

			// reassign the properties with correspoding new value
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Succefully updated!");

			document.querySelector("#txt-edit-title").value = null;
			document.querySelector("#txt-edit-body").value = null;
			document.querySelector("#btn-submit-update").setAttribute("disabled", true)
			// stop the loop once the changes are made
			break;

		}
	}
})

const deletePost = (id) => {
  posts.splice(id - 1, 1);
  document.getElementById(`post-${id}`).remove();
  alert("Post deleted")
  showPosts(posts)
};
